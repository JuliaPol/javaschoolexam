package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */

    private Map<String, Integer> treeMap;

    public boolean process(File sourceFile, File targetFile) {
        if (sourceFile == null || targetFile == null || !sourceFile.exists()) {
            throw new IllegalArgumentException();
        }
        try {
            if (!targetFile.exists()) {
                targetFile.createNewFile();
            }
            try (BufferedReader in = new BufferedReader(new FileReader(sourceFile));
                 BufferedWriter out = new BufferedWriter(new FileWriter(targetFile, true))) {
                String s;
                treeMap = new TreeMap<>();
                while ((s = in.readLine()) != null) {
                    addElem(s);
                }

                for (Map.Entry<String, Integer> entry : treeMap.entrySet()) {
                    out.write(getString(entry) + "\n");
                    out.flush();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void addElem(String s) {
        int val = 1;
        for (Map.Entry<String, Integer> entry : treeMap.entrySet()) {
            if (s.equals(entry.getKey())) {
                val = entry.getValue();
                val++;
            }
        }
        treeMap.put(s, val);
    }

    public String getString(Map.Entry<String, Integer> entry) {
        return entry.getKey() + "[" + entry.getValue() + "]";
    }


}
