package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        } else if ((x.isEmpty() && !y.isEmpty()) || (x.isEmpty() && y.isEmpty())) {
            return true;
        } else if ((!x.isEmpty() && y.isEmpty()) || (x.size() > y.size())) {
            return false;
        } else {
            int pos = 0;
            for (Object e : y) {
                if (pos >= x.size()) {
                    return true;
                }
                if (e.equals(x.get(pos))) {
                    pos++;
                }
            }
            return pos == x.size();
        }
    }
}

