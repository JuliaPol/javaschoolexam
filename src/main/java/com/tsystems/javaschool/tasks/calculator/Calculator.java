package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.ArrayDeque;
import java.util.Deque;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     * parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    private static final String FORMAT = "#0.####";
    private static final String[] OPERATORS = {"+", "-", "*", "/", "(", ")"};
    private Deque<Double> deqNumb;
    private Deque<Character> deqOper;

    public String evaluate(String statement) {
        if (statement != null && checkBrackets(statement)) {
            statement = conversionString(statement.replace(" ", ""));
            String arrStrings[] = statement.split(" ");
            deqNumb = new ArrayDeque<>();
            deqOper = new ArrayDeque<>();
            try {
                fillStack(arrStrings);
                while (!deqOper.isEmpty()) {
                    calculate(deqNumb, deqOper.removeLast());
                }
                DecimalFormat myFormatter = new DecimalFormat(FORMAT);
                statement = myFormatter.format(deqNumb.getFirst());
                return statement.replace(",", ".");
            } catch (RuntimeException ex) {
                return null;
            }
        } else return null;
    }

    public boolean checkBrackets(String s) {
        int countBrackets = 0;
        char line[] = s.toCharArray();
        for (char elem : line) {
            if (elem == '(') countBrackets++;
            if (elem == ')') countBrackets--;
        }
        return countBrackets == 0;
    }

    public String conversionString(String s) {
        for (String s1 : OPERATORS) {
            s = s.replace(s1, " " + s1 + " ");
        }
        return s;
    }

    public boolean isOperator(String s) {
        return s.equals("+") || s.equals("-") || s.equals("/") || s.equals("*");
    }

    public int priority(char operator) {
        switch (operator) {
            case '*':
            case '/':
                return 1;
            case '+':
            case '-':
                return 0;
            default:
                return -1;
        }
    }

    public void fillStack(String s[]) {
        for (String s1 : s) {
            if (s1.equals("(")) {
                deqOper.add(s1.charAt(0));
            } else if (s1.equals(")")) {
                while (deqOper.getLast() != '(') {
                    calculate(deqNumb, deqOper.removeLast());
                }
                deqOper.removeLast();
            } else if (isOperator(s1)) {
                while (!deqOper.isEmpty() &&
                        priority(deqOper.getLast()) >= priority(s1.charAt(0))) {
                    calculate(deqNumb, deqOper.removeLast());
                }
                deqOper.add(s1.charAt(0));
            } else if (!s1.equals("")) {
                deqNumb.add(Double.parseDouble(s1));
            }
        }
    }

    public void calculate(Deque<Double> st, char operator) {
        double someOne = st.removeLast();
        double someTwo = st.removeLast();
        switch (operator) {
            case '+':
                st.add(someTwo + someOne);
                break;
            case '-':
                st.add(someTwo - someOne);
                break;
            case '*':
                st.add(someTwo * someOne);
                break;
            case '/':
                if (someOne == 0) throw new ArithmeticException();
                else st.add(someTwo / someOne);
                break;
            default:
                break;
        }
    }

}

